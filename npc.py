import pygame
import vector
import spatialh
class thingthatspins (object):
	def __init__(self,x):
		self.colour = (235,20,50)
		self.SHAPE = "CIRC"
		self.FLAGS = ["MOBI"]
		self.spd = 2
		self.rad = 10
		self.dir = 180
		self.uv = vector.uvturn(self.dir)
		self.pos =[500,300.0]
		self.rect = pygame.Rect(-self.rad*2,-self.rad*2, self.rad*2,self.rad*2)
		self.rect.center = self.pos
		
		self.hashes = []
		self.hashpend()
	def move(self):
		from main import updates, surf_obj, surf_bgr, MainWindow
		self.hashmove()
		updates.append(self.rect.copy())	
		self.dir += self.spd
		if self.dir <= 0:
			self.dir = 360
		elif self.dir > 360:
			self.dir = 1
		self.uv = vector.uvturn(self.dir)
		self.pos = (self.pos[0]+self.uv[0]*self.spd ,self.pos[1]+self.uv[1]*self.spd)
		self.rect.center = [int(self.pos[0]),int(self.pos[1])]
		self.hashpend()
		for hash in self.hashes: 
			if len(hash) > 1:
				for obj in hash:
					if not obj == self:
						if obj.SHAPE == "CIRC":
							dis = vector.vector_distance(self.rect.center,obj.rect.center)
							if (dis < self.rad+obj.rad):
								vector.circ_circ(self,obj,dis)
						elif obj.rect.colliderect(self.rect):
							self.pos = (self.pos[0]+self.uv[0]*self.spd ,self.pos[1]+self.uv[1]*self.spd)
							self.rect.center = [int(self.pos[0]),int(self.pos[1])]
							
		pygame.draw.circle(surf_obj, self.colour, self.rect.center, self.rad, 0)
		MainWindow.blit(surf_obj,self.rect.topleft,self.rect)
		updates.append(self.rect.copy())	
	def step(self):
		pass
	def hashpend(self):
		from main import hash
		start = [self.rect.topleft[0]/hash.size,	self.rect.topleft[1]/hash.size]
		xdif = self.rect.right/hash.size -self.rect.left/hash.size
		ydif = self.rect.bottom/hash.size -self.rect.top/hash.size 
		for x in range(0,xdif+1):
			for y in range(0,ydif+1):
				hash.hash[start[0]+x][start[1]+y].append(self)
				self.hashes.append(hash.hash[start[0]+x][start[1]+y])
	def hashmove(self):
		for hash in self.hashes:
			hash.remove(self)
		self.hashes=[]
		
		
		
		
		
		
		
		
		
		
		
		