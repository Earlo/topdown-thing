# coding: utf-8
#This is the main file. Run this, to run the program
import random
import pygame
from pygame.locals import *
import math
import sys

import environment
import player
import npc
import spatialh
#Setup
pygame.init()
pygame.font.init()
#colors
black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
# set up fonts
basicFont = pygame.font.SysFont(None, 48)

FPS = 60 # 60 frames per second
clock = pygame.time.Clock()


done = False
font = pygame.font.SysFont("Calibri", 15)

#window and setting up display stuff
SWIDTH =  1000
SHEIGTH = 600
global MainWindow
MainWindow = pygame.display.set_mode((SWIDTH, SHEIGTH))
surf_obj = pygame.Surface((SWIDTH, SHEIGTH))
surf_dar = pygame.Surface((SWIDTH, SHEIGTH))
surf_bgr = pygame.Surface((SWIDTH, SHEIGTH))
surf_dar.fill((50,50,50))
surf_dar.set_colorkey((0,0,0),0)
surf_bgr.fill(white)
surf_obj.fill(white)


#setting up spatial hash
size = 25
hash = spatialh.spatialhash(SWIDTH, SHEIGTH,size)
hash.debug()
MainWindow.blit(surf_bgr,(0,0))
#pygame.display.flip()


updates = []
PLAYER = player.player()
NPC = []
for x in range(0,0):
	NPC.append(npc.thingthatspins(x))
W = environment.wall
walls = [W((20,20,200,10)),W((260,20,200,10)),W((500,20,200,10)),W((30,20,10,300)),W((400,400,400,20))]
for wall in walls:
	wall.draw()
	
MainWindow.blit(surf_bgr,(0,0))
pygame.display.flip()
while not done:
	for event in pygame.event.get(): # User did something
		if event.type == pygame.QUIT: # Closed from X
			done = True # Stop the Loop
	keys = pygame.key.get_pressed()
	for place in updates:
		surf_obj.blit(surf_bgr,place.topleft,place)
		MainWindow.blit(surf_bgr,place.topleft,place)
	updates = []

	#debug 4
	hash.debug()
	
	
	MainWindow.blit(surf_bgr,(0,0))
	for wall in walls:
		wall.draw()#for now drawn in here, eventually anything that is seen will be first calculated in players step funk and after that drawn
	for npc in NPC:
		npc.move()
	
	offset = [0,0]
	if keys[K_LEFT]:
		offset[0] = 1
	elif keys[K_RIGHT]:
		offset[0] = -1
	if keys[K_UP]:
		offset[1] = 1
	elif keys[K_DOWN]:
		offset[1] = -1
	#if not offset == [0,0]:
	PLAYER.move(offset)
	#else:
	#	player.step()


	MainWindow.blit(surf_dar,(0,0))
	#pygame.display.update(updates)
	pygame.display.flip()
	surf_dar.fill((50,50,50))
	clock.tick(FPS)
	pygame.display.set_caption("FPS: %i" % clock.get_fps())

sys.exit(0)







