import pygame
class wall(object):
	def __init__(self,rect):
		self.rect = pygame.Rect(rect)
		self.colour = (0,0,0)
		self.hashes = []
		self.hashpend()
		self.SHAPE = "RECT"
		self.FLAGS = ["STAT"]
	def draw(self):
		from main import updates, surf_obj, MainWindow
		pygame.draw.rect(surf_obj, self.colour, self.rect, 0)
		MainWindow.blit(surf_obj,self.rect.topleft,self.rect)
		updates.append(self.rect)
	def hashpend(self):
		from main import hash
		start = [self.rect.topleft[0]/hash.size,	self.rect.topleft[1]/hash.size]
		xdif = self.rect.right/hash.size -self.rect.left/hash.size
		ydif = self.rect.bottom/hash.size -self.rect.top/hash.size 
		for x in range(0,xdif+1):
			for y in range(0,ydif+1):
				hash.hash[start[0]+x][start[1]+y].append(self)
				self.hashes.append(hash.hash[start[0]+x][start[1]+y])