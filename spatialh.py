import pygame
import vector
class spatialhash(object):
	def __init__(self,x,y,s):
		self.size = s
		xlen = x/s
		ylen = y/s
		self.hash = []
		for x in range(0,xlen):
			self.hash.append([])
			for y in range (0,ylen):
				self.hash[x].append([])
	def debug(self):
		size = self.size
		from main import surf_bgr
		for x in range(0,len(self.hash)):
			for y in range(0,len(self.hash[x])):
				if len(self.hash[x][y]) == 0:
					pygame.draw.rect(surf_bgr, (230,230,230),(x*size, y*size ,size ,size),1)
				else:
					pygame.draw.rect(surf_bgr, (230,130,130),(x*size, y*size ,size ,size),1)

	
