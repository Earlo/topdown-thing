import math
from numpy import *

def simple_distance(pos1,pos2):
	return math.sqrt((pos1[0]-pos2[0])**2 + (pos1[1]-pos2[1])**2)
    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vector_distance (pos,tar):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	return math.fabs(math.sqrt(vecx**2+vecy**2))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvector(s,e):
	#print (s,e)
	vecx = (e[0] - s[0])
	vecy = (e[1] - s[1])
	length = (math.sqrt(vecx**2+vecy**2))
	unitvector = (vecx/length, vecy/length)
	return unitvector
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvectorl(s,e):
	vecx = (s[0] - e[0])
	vecy = (s[1] - e[1])
	length = (math.sqrt(vecx**2+vecy**2))
	try:
		unitvector = (vecx/length, vecy/length,length)
	except ZeroDivisionError:
		unitvector = (1,1)
	return unitvector
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvturn(dir): #this thing is for circular movement of a unitvector
	dir = math.radians(dir)
	vec = [math.sin(dir),math.cos(dir)]
	return vec
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vpoint(pos,vec,len): #returns point at the end of vector vec of length len
	return [pos[0]+vec[0]*len,	pos[1]+vec[1]*len]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vpointnormal(pos,vec,len): #returns point at the end of vector vec's normal of length len
	return [pos[0]-vec[1]*len,	pos[1]+vec[0]*len]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def ssift(pos,tar,dist):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	v = (vecx*dist,vecy*dist)
	return v
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sift(pos,tar,len):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	length = math.fabs(math.sqrt(vecx**2+vecy**2))
	unitvector = ((vecx/length)*len, (vecy/length)*len)
	newx = int (round (pos[0] - unitvector[0]))
	newy = int (round (pos[1] - unitvector[1]))
	return (newx,newy)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def ccw(A,B,C):
	return (C[1]-A[1])*(B[0]-A[0]) > (B[1]-A[1])*(C[0]-A[0])
def seg_intersect(A, B):
	return ccw(A[0],B[0],B[1]) != ccw(A[1],B[0],B[1]) and ccw(A[0],A[1],B[0]) != ccw(A[0],A[1],B[1])
	
#collision detections and stuff
def circ_circ(A,B,dis):
	overlap = (A.rad+B.rad)-dis
	dir= uvectorl(A.rect.center,B.rect.center)
	len= overlap/2
	if "STATIONARY" in A.FLAGS:
		B.pos = vpoint(B.rect.center,dir,-len*2)
	elif "STATIONARY" in B.FLAGS:
		A.pos = vpoint(A.rect.center,dir,len*2)
	else:
		B.pos = vpoint(B.rect.center,dir,-len*2)
		A.pos = vpoint(A.rect.center,dir,len*2)
	
def rect_rect(rect,p,rad):
	return rect.collidepoint(p)

	
#fuckingfuckfuck, i am just too dumb to get this work : P
"""def rect_circ(rect,p,rad):
	return rect.collidepoint(p) or
		line_circ((rect.topleft,rect.topright)p,rad) or
		line_circ((rect.topright,rect.bottomright)p,rad)) or
		line_circ((rect.bottomright,rect.bottomleft)p,rad) or
		line_circ((rect.bottomleft,rect.topleft)p,rad)
def line_circ(line,p,rad):
	seg_v [line[0][0]-line[1][0],line[0][1]-line[1][1]]
	pt_v = [line[0][0]-p[0],line[0][1]-p[1]]""" 