import pygame
import vector
class player (object):
	def __init__(self):
		self.vision = 100
		self.colour = (25,200,50,255)
		self.SHAPE = "CIRC"
		self.FLAGS = ["MOBI","PLAYER"]
		self.spd = 3
		self.rad = 10
		self.fow = 120
		self.dir = 180
		self.uv = vector.uvturn(self.dir)
		self.pos =[200.0,200.0]
		self.rect = pygame.Rect(-self.rad*2,-self.rad*2, self.rad*2,self.rad*2)
		self.rect.center = self.pos
		
		self.hashes = []
		self.hashpend()
	def move(self,os):
		from main import updates, surf_obj, surf_dar, MainWindow
		self.hashmove()
		updates.append(self.rect.copy())
		if not os[0] == 0:
			if not os[1] == 0:
				self.dir += os[0]*self.spd
			else:
				self.dir += os[0]*self.spd*3
			if self.dir <= 0:
				self.dir += 360
			elif self.dir > 360:
				self.dir -= 360
			self.uv = vector.uvturn(self.dir)
		if not os[1] == 0:
			self.pos = (self.pos[0]+self.uv[0]*os[1]*self.spd ,self.pos[1]+self.uv[1]*os[1]*self.spd)
		self.rect.center = [int(self.pos[0]),int(self.pos[1])]
		self.hashpend()
		for hash in self.hashes:
			if len(hash) > 1:
				for obj in hash:
					if not obj == self:
						if obj.SHAPE == "CIRC":
							dis = vector.vector_distance(self.rect.center,obj.rect.center)
							if (dis < self.rad+obj.rad):
								vector.circ_circ(self,obj,dis)
						elif obj.rect.colliderect(self.rect):
							self.pos = (self.pos[0]+self.uv[0]*-os[1]*self.spd ,self.pos[1]+self.uv[1]*-os[1]*self.spd)
							self.rect.center = [int(self.pos[0]),int(self.pos[1])]
		lv = vector.uvturn(self.dir+60)
		rv = vector.uvturn(self.dir-60)
		print (lv,rv)			
		pygame.draw.circle(surf_obj, self.colour, self.rect.center, self.rad, 0)
		pygame.draw.line(surf_obj,(255,20,20,20),self.pos,vector.vpoint(self.pos,self.uv,self.rad-1),1)
		#MainWindow.blit(surf_obj,(0,0))
		pygame.draw.polygon(surf_dar, (0,0,0,0), (self.rect.center,vector.vpoint(self.pos,lv,2000),vector.vpoint(self.pos,rv,2000)), 0)	
		MainWindow.blit(surf_obj,self.rect.topleft,self.rect)
		updates.append(self.rect.copy())	
	def step(self):
		pass
	def hashpend(self):
		from main import hash
		start = [self.rect.topleft[0]/hash.size,	self.rect.topleft[1]/hash.size]
		xdif = self.rect.right/hash.size -self.rect.left/hash.size
		ydif = self.rect.bottom/hash.size -self.rect.top/hash.size 
		for x in range(0,xdif+1):
			for y in range(0,ydif+1):
				hash.hash[start[0]+x][start[1]+y].append(self)
				self.hashes.append(hash.hash[start[0]+x][start[1]+y])
	def hashmove(self):
		for hash in self.hashes:
			hash.remove(self)
		self.hashes=[]
		
		
		
		
		
		
		
		
		
		
		